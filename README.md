***201020214 - Tikiram Samaneb Ruiz Manuel***

# ADS - Tarea 1
## Repositorio

https://gitlab.com/tikiram/ads-tarea-01

## Commit

* Se clona el repo

![01](img/05.png)

* se edita el `README.md`

![01](img/04.png)

* Se agrega algo de contenido

![01](img/03.png)

* Se realiza el primer commit

![01](img/02.png)

* Se realiza push a `origin master`

![01](img/01.png)

## Git Commands

* git stash
* git stash pop
* git stash list
* git pull origin master
* git log
* git rm
* git add .
* git commit -m 'asdf
* git pull origin master --no-edit

Algunas de mis notas

https://github.com/Tikiram/notes/blob/master/git.md
